package com.ucbcba.blog.repositories;

import org.springframework.data.repository.CrudRepository;
import com.ucbcba.blog.entities.User;
import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User,Integer> {
}
